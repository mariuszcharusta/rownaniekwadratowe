import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class testRownanieKwadratowe {
    static RownanieKwadratowe rk;

    @BeforeAll
    public static void setup(){
        rk = new RownanieKwadratowe(1,10,3);
    }

    @Test
    public void testObliczDelta(){
        //when
        double wynikDelta = rk.obliczDelta();
        //then
        assertEquals(88, wynikDelta);
    }

    @Test
    public void obliczX1(){
        //when
        double wynikDelta = rk.obliczDelta();
        double x1= rk.obliczX1(wynikDelta);
        //then
        assertEquals(-0.30958424017657027, x1);
    }

    @Test
    public void obliczX2(){
        //when
        double wynikDelta = rk.obliczDelta();
        double x1= rk.obliczX2(wynikDelta);
        //then
        assertEquals(-9.69041575982343, x1);
    }

    @Test
    public void sprawdzWynik(){
        //when
        List<Double> wynikRownaniaKwadratowego = rk.podajWynik();
        //then
        assertTrue(wynikRownaniaKwadratowego.size()==2);
    }
}
