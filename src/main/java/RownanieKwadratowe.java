import java.util.ArrayList;
import java.util.List;

public class RownanieKwadratowe {
    private int a;
    private int b;
    private int c;

    public RownanieKwadratowe(int a, int b, int c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public double obliczDelta(){
        return Math.pow(b,2)-4*a*c;
    }

    public double obliczX0(){
        return (-b)/(2*a);
    }

    public double obliczX1(double delta){
        return (-b+Math.sqrt(delta))/(2*a);
    }

    public double obliczX2(double delta){
        return (-b-Math.sqrt(delta))/(2*a);
    }

    public List<Double> podajWynik(){
        double delta=obliczDelta();
        List<Double> listaZWynikami = new ArrayList<Double>();
        if (delta>0){
            double x1=obliczX1(delta);
            double x2=obliczX2(delta);
            listaZWynikami.add(x1);
            listaZWynikami.add(x2);
//            tablica[0]=x1
            return listaZWynikami;
        }
        else if(delta ==0){
            double x0 = obliczX0();
            listaZWynikami.add(x0);
            return listaZWynikami;
        }
        else {
            throw new ArithmeticException("Delta jest mniejsza od 0");
        }
    }
}
