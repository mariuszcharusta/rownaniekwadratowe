import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class NaukaMap {

    public static void main(String[] args) {
        Map<String, List<String>> mojaMapa = new HashMap<>();
        List<String> imiona=new ArrayList<>();
        imiona.add("Jan");
        imiona.add("Ala");
        List<String> nazwiska = new ArrayList<>();
        nazwiska.add("CHarusta");
        mojaMapa.put("imiona", imiona);
        mojaMapa.put("nazwisko", nazwiska);
        System.out.println(mojaMapa);
        System.out.println(mojaMapa.get("imiona"));
        mojaMapa.get("imiona").add("Tomasz");
        System.out.println(mojaMapa);

    }

}
